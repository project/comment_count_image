
Comment Count Image -
Adds a dynamically-generated image to RSS feed items
that shows the number of comments on that node.


SHORT DESCRIPTION
-----------------
RSS feeds are only updated once in a while by feed readers. Comments might be
updated anytime. Still, it would be cool to show the current number of comments
on a given page, blog post, or whatever content. The feed-loving user can then
tell in a glimpse if visiting the page will pay off, if there are comments
at all or if there are new comments compared to an earlier visit on that page.

This module provides this functionality by means of an image whose URL stays
the same for each post (thus not requiring the feed itself to be updated).
The image itself though always reflects the current number of comments.

Once activated, the module automatically adds the comment count images to each
feed item if comments are enabled for the corresponding node.
No further configuration options are provided (and not necessary either).


USING TRANSLATIONS
------------------

If you want to have the comment count image translated into your language
instead of plain English, you start by importing a .po (translation) file
or translating the strings directly in Drupal's administrative interface,
as you would do normally with translations.

When the strings are entered, any (English-labeled) images that might already
exist in the comment-count-image subdirectory inside your file directory need
to be deleted. The module will then create the appropriate images anew in the
site language when the feed is being accessed.


AUTHOR
------
Jakob Petsovits ("jpetso", http://drupal.org/user/56020)


THANKS TO
---------
Wordpress for providing the idea for this functionality.
In fact, this module is a clone of the same functionality in Wordpress,
although the code has been written from scratch in a clean-room fashion.
